#ifndef FSM_HEADER
#define FSM_HEADER

#include "pid.hh"

class FSM
{
public:
    FSM();

    bool run();

private:
    void poll();
    void proc();
    void writ();

    PID a, b, c, d;
    double curAngle;
};

#endif
