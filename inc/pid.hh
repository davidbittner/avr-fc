#ifndef PID_HEADER
#define PID_HEADER

/*
 * This class is for creating a PID feedback loop controller.
 */

class PID 
{
public:
    //Constructor for setting the gains, as well as the target
    PID( double pg, double ig, double dg, double target );

    //Passes the current rotation angle in radians
    void   poll(double raw);
    //Processses the P, I, and D
    void   proc();
    //Returns the calculated PID value
    double sum();

private:
    double pg, ig, dg;
    double p, i, d;

    double target;

    double     err;
    double prevErr;
};

#endif
