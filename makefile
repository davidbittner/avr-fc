C = avr-gcc

SRC = $(wildcard ./src/*.cc)
INC = $(wildcard ./inc/*.hh)

OBJS = $(SRC:.cc=.o)
DEPFILES = $(SRC:.cc=.d)

BIN = run.bin
HEX = $(subst bin,hex,$(BIN))

MMCU        = atmega328p

AVRDUDEMMCU = m328p
DEV         = /dev/ttyACM0
PROGRAMMER  = arduino

FLAGS = -Wall -Werror -g -Iinc/ -MMD -O3 -mmcu=$(MMCU)
LFLAGS =

all: build flash

build: $(OBJS)
	@echo "COMPILING"
	@echo "------------------------------------"
	@$(C) --version
	@$(C) $(FLAGS) $(LFLAGS) $(OBJS) -o $(BIN)
	@echo ""
	@echo "$(BIN) generated successfully."
	@echo "------------------------------------"

flash:
	@echo ""
	@echo "FLASHING"
	@echo "------------------------------------"
	@avr-objcopy -j .text -j .data -O ihex $(BIN) $(HEX)
	@avrdude -c $(PROGRAMMER) -p $(AVRDUDEMMCU) -P $(DEV) -U flash:w:$(HEX)
	@echo "------------------------------------"

-include $(DEPFILES)

%.o: %.cc
	@echo "Building $<"
	@$(C) $(FLAGS) -c $< -o $@
	@echo "Built $< to $@."

clean:
	@rm -f $(OBJS)
	@rm -f $(BIN)
	@rm -f $(DEPFILES)
	@rm -f $(HEX)
	@echo Cleaned.

force:
	@touch $(SRC) $(INC)
	@make all
