#include "pid.hh"

PID::PID( double pg, double ig, double dg, double target )
{
    this->pg =     pg;
    this->ig =     ig;
    this->dg =     dg;
    this->target = target;

    prevErr = 0.0;
    err     = 0.0;
    p       = 0.0;
    i       = 0.0;
    d       = 0.0;
}

void PID::poll(double raw)
{
    err = raw - target;
}

void PID::proc()
{
    p  =  err * pg;
    i +=  err * ig;
    d  = (err - prevErr)*dg;
}

double PID::sum()
{
    return p + i + d;
}
