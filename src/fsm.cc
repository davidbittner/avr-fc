#include "fsm.hh"

FSM::FSM() :
    a(0, 0, 0, 0),
    b(0, 0, 0, 0),
    c(0, 0, 0, 0),
    d(0, 0, 0, 0)
{
    curAngle = 0.0;
}

void FSM::poll()
{
    a.poll(curAngle);
    b.poll(curAngle);
    c.poll(curAngle);
    d.poll(curAngle);
}

void FSM::proc()
{
    a.proc();
    b.proc();
    c.proc();
    d.proc();
}

void FSM::writ()
{
    //Write to PWM lines here
}

//Isolated due to not wanting to overload call stack
bool FSM::run()
{
    poll();
    proc();
    writ();

    return true;
}
